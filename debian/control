Source: emacs-memoize
Section: lisp
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders: Lev Lamberov <dogsleg@debian.org>
Build-Depends: debhelper-compat (= 12),
               dh-elpa
Standards-Version: 4.3.0
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-elpa
Homepage: https://github.com/skeeto/emacs-memoize
Vcs-Browser: https://salsa.debian.org/emacsen-team/emacs-memoize
Vcs-Git: https://salsa.debian.org/emacsen-team/emacs-memoize.git

Package: elpa-memoize
Architecture: all
Depends: ${elpa:Depends},
         ${misc:Depends}
Recommends: emacs (>= 46.0)
Enhances: emacs
Description: memoization functions
 This package provides memoization functions. `memoize' accepts a
 symbol or a function. When given a symbol, the symbol's function
 definition is memoized and installed overtop of the original function
 definition. When given a function, it returns a memoized version of
 that function.
 .
 Memoization takes up memory, which should be freed at some point.
 Because of this, all memoization has a timeout from when the last
 access was.
